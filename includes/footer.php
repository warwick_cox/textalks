<footer class="row">
    <div class="large-12 columns"><hr/>
        <div class="row">
            
            <div class="large-6 columns">
                <p>© Copyright Team Auroras - Brisbane.</p>
            </div>
            
            <div class="large-6 columns">
                <ul class="inline-list right">
                    <li><a href="/home.php">Home</a></li>
                    <li><a href="/talks.php">Talks</a></li>
                    <li><a href="/tips.php">Tips</a></li>
                </ul>
            </div>
            
        </div>
    </div>
</footer>
