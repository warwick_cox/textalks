<header id="header" class="row">

    <a class="logo_primary_font logo_main" href="/">TEX Talks</a>
    <div class="logo_secondary_font motto">Worth practicing</div>
    <div class="clearfix"></div>
<div class="clearfix">
    <h5 class="subheader saying"><i>"The only way to be a great speaker is to fail at being a poor one" Someone great</i></h5>
</div>
</header><!-- /header -->
